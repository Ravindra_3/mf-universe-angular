import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Observable } from 'rxjs';
import { funds } from './funds';
import { throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class MutualFundService {
  public _url = 'https://api.kuvera.in/api/v3/funds.json';

  constructor(private http: HttpClient) {}

  getdata(): Observable<funds[]> {
    return this.http.get<funds[]>(this._url).pipe(

      catchError(this.handleError)
    );
  }
  handleError(error) {
    let errorMessage = '';

    if (error.error instanceof ErrorEvent) {
      errorMessage = `Error: ${error.error.message}`;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }

    return throwError(errorMessage);
  }
}
