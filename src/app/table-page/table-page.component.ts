import { Component, OnInit } from '@angular/core';
import { MutualFundService } from '../mutual-fund.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-table-page',
  templateUrl: './table-page.component.html',
  styleUrls: ['./table-page.component.css'],
})
export class TablePageComponent implements OnInit {
  public data;
  public modefieddata;
  public totaldata;
  public uparrow = true;
  public hideuparrow = false;
  public hidedownarrow = false;
  public message = '';
  public errormessage;
  public filter1 = 'undefined';
  public filter2 = 'undefined';
  public filter3 = 'undefined';
  public plan;
  public fund_type;
  public datacategaroy;
  public count7;

  constructor(
    private _mutualfundService: MutualFundService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  // fetching data
  ngOnInit(): void {
    this._mutualfundService.getdata().subscribe(
      (data1) => {
        this.data = data1;
        var count = {};
        var plan = {};
        var fundtype = {};

        this.data.map((e) => {
          if (e.plan == null) {
            e['plan'] = 'NA';
          }

          if (e.fund_type == null) {
            e['fund_type'] = 'NA';
          }
          if (e.fund_category == null) {
            e['fund_category'] = 'NA';
          }

          if (e.returns['year_3'] == null) {
            e.returns['year_3'] = 'NA';
          }
          if (typeof e.returns['year_3'] !== 'number') {
            e.returns['year_3'] = 'NA';
          }
          if (typeof e.returns['year_3'] == 'undefined') {
            e.returns['year_3'] = 'NA';
          }
          if (typeof e.returns['year_1'] !== 'number') {
            e.returns['year_3'] = 'NA';
          }
          if (typeof e.returns['year_1'] == 'undefined') {
            e.returns['year_1'] = 'NA';
          }

          count[e.fund_category] = count[e.fund_category]
            ? count[e.fund_category] + 1
            : 1;
          plan[e.plan] = plan[e.plan] ? plan[e.plan] + 1 : 1;
          fundtype[e.fund_type] = fundtype[e.fund_type]
            ? fundtype[e.fund_type] + 1
            : 1;
        });

        this.plan = Object.keys(plan);
        this.count7 = Object.values(count);
        this.datacategaroy = Object.keys(count);

        let count4 = this.count7.reduce(function (a, b) {
          return a + b;
        }, 0);
        console.log(count4);
        this.totaldata = data1;
        this.totaldata = this.data;
      },
      (error) => {
        this.errormessage = 'Internal Server Error';
      }
    );
  }

  // plan field sort

  plansort() {
    return this.data.sort((a, b) => {
      return a.plan > b.plan ? 1 : -1;
    });
  }

  // Asc year3 return feild sort
  returnsort() {
    return this.data.sort((a, b) => {
      if (a.returns['year_3'] == 'NA' && b.returns['year_3'] == 'NA') return 0;
      else if (a.returns['year_3'] == 'NA' && b.returns['year_3'] != 'NA')
        return 1;
      else if (a.returns['year_3'] != 'NA' && b.returns['year_3'] == 'NA')
        return -1;
      else {
        return a.returns['year_3'] < b.returns['year_3'] ? 1 : -1;
      }
    });
  }

  //Desc year3 return feild sort

  returnsort1() {
    return this.data.sort((a, b) => {
      if (a.returns['year_3'] == 'NA' && b.returns['year_3'] == 'NA') return 0;
      else if (a.returns['year_3'] == 'NA' && b.returns['year_3'] != 'NA')
        return -1;
      else if (a.returns['year_3'] != 'NA' && b.returns['year_3'] == 'NA')
        return 1;
      else {
        return a.returns['year_3'] < b.returns['year_3'] ? -1 : 1;
      }
    });
  }

  // Asc year1 return feild sort
  returnsortyear1() {
    return this.data.sort((a, b) => {
      if (a.returns['year_1'] == 'NA' && b.returns['year_1'] == 'NA') return 0;
      else if (a.returns['year_1'] == 'NA' && b.returns['year_1'] != 'NA')
        return 1;
      else if (a.returns['year_1'] != 'NA' && b.returns['year_1'] == 'NA')
        return -1;
      else {
        return a.returns['year_1'] < b.returns['year_1'] ? 1 : -1;
      }
    });
  }

  // desc year1 return feild sort
  returnsortyear2() {
    return this.data.sort((a, b) => {
      if (a.returns['year_1'] == 'NA' && b.returns['year_1'] == 'NA') return 0;
      else if (a.returns['year_1'] == 'NA' && b.returns['year_1'] != 'NA')
        return -1;
      else if (a.returns['year_1'] != 'NA' && b.returns['year_1'] == 'NA')
        return 1;
      else {
        return a.returns['year_1'] < b.returns['year_1'] ? -1 : 1;
      }
    });
  }

  // sorting function
  action(val) {
    switch (val) {
      case 'name-asc':
        return this.data.sort((a, b) => (a.name > b.name ? 1 : -1));

      case 'fund_category':
        return this.data.sort((a, b) =>
          a.fund_category > b.fund_category ? 1 : -1
        );
      case 'fund_type':
        return this.data.sort((a, b) => (a.fund_type > b.fund_type ? 1 : -1));
      case 'plan':
        this.plansort();
        break;
      case 'year_1':
        this.returnsortyear1();

        break;
      case 'year_3':
        this.returnsort();

        break;
    }
  }
  // Navigate to detail page

  detail(event) {
    let code = event.target.id;
    this.router.navigate(['/detail', code]);
  }
  gotohome() {
    this.router.navigate(['/']);
  }

  // filter function

  selected(event) {
    let name = event.target.id;
    let value = event.target.value;

    if (name == 'fund_category') {
      this.filter1 = value;
    }

    if (name == 'plan') {
      this.filter2 = value;
    }
    if (name == 'fund_type') {
      this.filter3 = value;
    }

    this.data = this.totaldata;

    if (
      this.filter1 !== 'undefined' &&
      this.filter2 == 'undefined' &&
      this.filter3 == 'undefined'
    ) {
      this.data = this.data.filter((e) => {
        return e.fund_category == this.filter1;
      });
    }

    if (
      this.filter2 !== 'undefined' &&
      this.filter1 == 'undefined' &&
      this.filter3 == 'undefined'
    ) {
      this.data = this.data.filter((e) => {
        return e.plan == this.filter2;
      });
    }

    if (
      this.filter3 !== 'undefined' &&
      this.filter1 == 'undefined' &&
      this.filter2 == 'undefined'
    ) {
      this.data = this.data.filter((e) => {
        return e.fund_type == this.filter3;
      });
    }

    //

    if (
      this.filter3 !== 'undefined' &&
      this.filter1 !== 'undefined' &&
      this.filter2 !== 'undefined'
    ) {
      this.data = this.data.filter((e) => {
        return e.fund_type == this.filter3;
      });

      this.data = this.data.filter((e) => {
        return e.plan == this.filter2;
      });

      this.data = this.data.filter((e) => {
        return e.fund_category == this.filter1;
      });
    }

    if (
      this.filter3 !== 'undefined' &&
      this.filter1 !== 'undefined' &&
      this.filter2 == 'undefined'
    ) {
      this.data = this.totaldata.filter((e) => {
        return true;
      });
      this.data = this.data.filter((e) => {
        return e.fund_type == this.filter3;
      });

      this.data = this.data.filter((e) => {
        return e.fund_category == this.filter1;
      });
    }

    if (
      this.filter1 !== 'undefined' &&
      this.filter2 !== 'undefined' &&
      this.filter3 == 'undefined'
    ) {
      this.data = this.totaldata.filter((e) => {
        return true;
      });

      this.data = this.data.filter((e) => {
        return e.plan == this.filter2;
      });

      this.data = this.data.filter((e) => {
        return e.fund_category == this.filter1;
      });

      // console.log('working3');
    }

    if (
      this.filter3 !== 'undefined' &&
      this.filter2 !== 'undefined' &&
      this.filter1 == 'undefined'
    ) {
      this.data = this.totaldata.filter((e) => {
        return true;
      });

      this.data = this.data.filter((e) => {
        return e.fund_type == this.filter3;
      });

      this.data = this.data.filter((e) => {
        return e.plan == this.filter2;
      });
    }

    if (this.data.length == 0) {
      this.message = 'No Matched Item Found';
    } else {
      this.message = '';
    }
  }

  // arrow1 direction change

  hidearrow1(val) {
    this.hideuparrow = !this.hideuparrow;
    this.hidedownarrow = false;

    switch (val) {
      case 'name-asc':
        return this.data.sort((a, b) => (a.name > b.name ? 1 : -1));

      case 'fund_category':
        return this.data.sort((a, b) =>
          a.fund_category > b.fund_category ? 1 : -1
        );
      case 'fund_type':
        return this.data.sort((a, b) => (a.fund_type > b.fund_type ? 1 : -1));
      case 'plan':
        this.plansort();
        break;
      case 'year_1':
        this.returnsortyear1();

        break;
      case 'year_3':
        this.returnsort();

        break;
    }
  }

  // arrow2 direction change
  hidearrow2(val) {
    this.hidedownarrow = !this.hidedownarrow;
    this.hideuparrow = false;

    switch (val) {
      case 'name-asc':
        return this.data.sort((a, b) => (a.name < b.name ? 1 : -1));

      case 'fund_category':
        return this.data.sort((a, b) =>
          a.fund_category < b.fund_category ? 1 : -1
        );
      case 'fund_type':
        return this.data.sort((a, b) => (a.fund_type < b.fund_type ? 1 : -1));
      case 'plan':
        return this.data.sort((a, b) => (a.plan < b.plan ? 1 : -1));

      case 'year_1':
        this.returnsortyear2();

        break;

      case 'year_3':
        this.returnsort1();
        break;
    }
  }
}
