import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailPageComponent } from './detail-page/detail-page.component'
import { TablePageComponent } from './table-page/table-page.component';
import { HomePageComponent } from './home-page/home-page.component'




const routes: Routes = [
  {path:'',component:HomePageComponent},
  {path:'explore',component:TablePageComponent},

 {path:'detail/:id',component:DetailPageComponent},
 {path:'**',component:HomePageComponent}
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
