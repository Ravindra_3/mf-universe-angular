import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail-page',
  templateUrl: './detail-page.component.html',
  styleUrls: ['./detail-page.component.css'],
})
export class DetailPageComponent implements OnInit {
  public data;
  public detail;
  public nameofscheme;
  public codename;
  public objective;
  public hidetable = false;

  public Scheme;
  Manager;
  type;
  category;
  FundHouse;
  FundName;
  Assets;
  errormsg;
  url;

  // private url = `https://api.kuvera.in/api/v3/funds/FR496-GR.json`;

  constructor(
    private route: ActivatedRoute,
    private http: HttpClient,
    private router: Router
  ) {}

  ngOnInit() {
    let code = this.route.snapshot.paramMap.get('id');
    this.url = `https://api.kuvera.in/api/v3/funds/${code}.json`;

    return this.http.get(this.url).subscribe(
      (data) => {
        this.data = data[0];
        let newdetail = Object.keys(data[0]);

        let filteritem = [
          'nav',
          'last_nav',
          'sips',
          'upsizecode_sip_dates',
          'sip_dates',
          'tags',
          'returns',
          'portfolio_turnover',
          'plan',
        ];
        filteritem.map((e) => {
          let index = newdetail.indexOf(e);
          newdetail.splice(index, 1);
        });
        this.detail = newdetail;

        this.FundName = this.data.fund_name;
        this.nameofscheme = this.data.name;
        this.FundHouse = this.data.fund_house;
        this.objective = this.data.investment_objective;
        this.category = this.data.fund_category;
        this.type = this.data.fund_type;
        this.Scheme = this.data.start_date;
        this.Manager = this.data.fund_manager;
        this.Assets = this.data.aum;
      },

      (error) => {
        this.errormsg = 'Internal Server Error';
        this.hidetable = true;
        // console.log(this.errormsg);
      }
    );
  }

  gotohome() {
    this.router.navigate(['/']);
  }
}
